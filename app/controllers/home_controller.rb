class HomeController < ApplicationController
  skip_before_action :authenticate_tenant!, :only => [ :index ] #don't not require user to be login

  def index
    if current_user
      if session[:tenant_id]
        Tenant.set_current_tenant session[:tenant_id]
      else
        Tenant.set_current_tenant current_user.tenants.first
      end

      @tenant = Tenant.current_tenant
      params[:tenant_id] = @tenant.id
    end
  end
end
